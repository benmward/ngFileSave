# ngFileSave
Ability to utilize HTML5 saveAs() as an AngularJS service call.


## License
[MIT ©](https://gitlab.com/benmward/angular-file-saver/blob/master/license.md)
